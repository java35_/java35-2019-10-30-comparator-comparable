
public class SortByColumn2 implements IComparator<Person> {
	public int compare(Person o1, Person o2) {
		return o1.column2.compareTo(o2.column2);
	}
}
