package homework;

import java.util.Comparator;

public class OddEvenComparator implements Comparator<Integer>{

	@Override
	public int compare(Integer o1, Integer o2) {
		// четное четное
		// четное нечетное
		// нечетное четное
		// нечетное нечетное
	
		// четное четное
		if (o1 % 2 == 0 && o2 % 2 == 0)			
			return o1 - o2;
		
		// четное нечетное
		if (o1 % 2 == 0 && o2 % 2 != 0)			
			return -1;
		
		// нечетное четное
		if (o1 % 2 != 0 && o2 % 2 == 0)			
			return 1;
		
		// нечетное нечетное
//		if (o1 % 2 != 0 && o2 % 2 == 0)			
		return Integer.compare(o2, o1);
		
		// 4 Variant
//		if (o1 % 2 == 0 && o2 % 2 == 0)			
//			return o1 - o2;
		
		// 3 Variant
//		if (o1 % 2 == 0 && o2 % 2 == 0) {
//			Integer.compare(o1, o2);
//		}
		
		
		// 2 Variant
//		if (o1 % 2 == 0 && o2 % 2 == 0) {
//			if (o1 > o2)
//				return 1;
//			if (o1 == o2)
//				return 0;
//			return -1;
//		}
		// 1 Variant
//			return o1.compareTo(o2);
	}
	
	// 5, 3, 8, 6, 9, 12
	// ==>
	// 6, 8, 12, 9, 5, 3

}
