package homework;

import java.util.Arrays;

public class HomeWorkAppl {

	public static void main(String[] args) {
		Integer[] array = {5, 3, 8, 6, 9, 12};
		// 5, 3, 8, 6, 9, 12
		// ==>
		// 6, 8, 12, 9, 5, 3
		
		OddEvenComparator oddEven = new OddEvenComparator();
		Arrays.sort(array, oddEven);
		
		for (Integer value : array) {
			System.out.print(value + " ");
		}
	}

}