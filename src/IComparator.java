
public interface IComparator<T> {
	public int compare(T o1, T o2);
}
