package interfaces;

public class Person implements ICryable {
	int id;
	String name;
	
	@Override
	public void cry() {
		System.out.println("Person cry!");
	}

	public void sayHello() {
		System.out.println("Hello!");
	}
}
