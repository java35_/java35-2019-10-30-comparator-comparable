package interfaces;

public class TestAppl {
	public static void main(String[] args) {
		Person person = new Person();
		Dog dog = new Dog();
		
		person.sayHello();
		person.cry();
		
		dog.gav();
		dog.cry();
		
		ICryable iCryable = dog;
	}
}
