public class MyArray<T extends IComparable<T>> {
	private Object[] array = new Object[10];
	private int size;
	
	public boolean add(T obj) {
		if (size > array.length)
			return false;
		array[size++] = obj;
		return true;
	}
	
	public T get(int index) {
		if (index < 0 || index >= size)
			return null;
		return (T)array[index];
	}
	
	public int getSize() {
		return size;
	}
	
	public void sort() {
		// Bubble sort
		// 0 1 2 3 4
		// 5 3 1 4 2
		// ----------
		// 3 5 1 4 2 - for i == 0
		// 3 1 5 4 2 - for i == 1
		// 3 1 4 5 2 - for i == 2
		// 3 1 4 2 5 - for i == 3
		// ---------
		// 3 1 4 2 5
		// ...
		// 1 3 2 4 5
		// ...
		// 1 2 3 4 5
		// 
		int length = 0;
		boolean sorted = false;
		while(!sorted) {
			sorted = true;
			for (int i = 0; i < size - 1 - length; i++) {
				if (get(i).compareTo(get(i + 1)) > 0) {
					Object tmpObj = get(i);
					array[i] = array[i + 1];
					array[i + 1] = tmpObj;
					sorted = false;
				}
			}
			length++;
		}
	}
	
	public void sortByColumn(IComparator<T> comparator) {
		int length = 0;
		boolean sorted = false;
		while(!sorted) {
			sorted = true;
			for (int i = 0; i < size - 1 - length; i++) {
//				if (get(i).compareTo(get(i + 1)) > 0) {
				if (comparator.compare(get(i), get(i + 1)) > 0) {
					Object tmpObj = get(i);
					array[i] = array[i + 1];
					array[i + 1] = tmpObj;
					sorted = false;
				}
			}
			length++;
		}
	}
}
