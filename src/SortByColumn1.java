
public class SortByColumn1 implements IComparator<Person> {
	public int compare(Person o1, Person o2) {
		return o1.column1.compareTo(o2.column1);
	}
}
