public class Person implements IComparable<Person> {
	int id;
	String column1;
	String column2;
	
	public Person(int id, String column1, String column2) {
		this.id = id;
		this.column1 = column1;
		this.column2 = column2;
	}

	public int getId() {
		return id;
	}

	public String getColumn1() {
		return column1;
	}

	public String getColumn2() {
		return column2;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", column1=" + column1 + ", column2=" + column2 + "]";
	}
	
	@Override
	public int compareTo(Person another) {
		if (getId() > another.getId())
			return 1;
		if (getId() == another.getId())
			return 0;
		return -1;
	}
}