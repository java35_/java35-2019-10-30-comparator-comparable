import java.util.Comparator;

public class TestAppl {
	public static void main(String[] args) {
		MyArray<Person> array = new MyArray<>();
		
		array.add(new Person(5, "E", "Q"));
		array.add(new Person(3, "B", "X"));
		array.add(new Person(1, "A", "Z"));
		array.add(new Person(4, "C", "W"));
		array.add(new Person(2, "D", "Y"));
		
		
		for (int i = 0; i < array.getSize(); i++) {
			System.out.println(array.get(i));
		}
		
//		IComparable<Person> comp = new Person(3, "B", "X");
//		Person person1 = new Person(3, "B", "X");
//		Person person2 = new Person(3, "B", "X");
//		comp.compareTo(person2);
		
		System.out.println();
		array.sort();
		for (int i = 0; i < array.getSize(); i++) {
			System.out.println(array.get(i));
		}
		
		
		System.out.println();
		SortByColumn1 sortByColumn1 = new SortByColumn1();
		array.sortByColumn(sortByColumn1);
		for (int i = 0; i < array.getSize(); i++) {
			System.out.println(array.get(i));
		}
		
		System.out.println();
		SortByColumn2 sortByColumn2 = new SortByColumn2();
		array.sortByColumn(sortByColumn2);
		for (int i = 0; i < array.getSize(); i++) {
			System.out.println(array.get(i));
		}
	}
}